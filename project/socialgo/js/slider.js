(function(jQuery){
    
     jQuery.fn.abDegisenArkaPlan = function(veriAkisi)
     {

        var varsayilan =
        {
            /*Yukarida gonderilen verileri burada aliyoruz*/
            resimlerArasiGecis			: 1000,
            resimEfekleri						: 1000,
            resimler								: "img/slider/resim1.png,img/slider/resim2.png,img/slider/resim3.png"
        };

        var ayarlar = jQuery.extend(varsayilan, veriAkisi);

        return this.each
        (
            function()
            {
                //Objeyi $obje degiskenine atiyoruz
                $obje       = $(this);
                //ekran genisligini ve yuksekligini aliyoruz.
                $genislik   = $(document).width();
                $yukseklik  = $(window).height();
                //virgul ile eklenen resimleri temizliyoruz.
                var resimler = varsayilan.resimler.split(",");
                //resimleri eklemek icin body tagi icine bir div ekliyoruz
                $obje.append($('<div/>').addClass('ab-resim-tasiyici-ab'));
                //eklentiye tanÄ±mlanan tum resimleri deminki div'in icine ekliyoruz
                for(var i in resimler)
                    $('.ab-resim-tasiyici-ab').append($('<img/>', {src: resimler[i]}).addClass('abResimler-ab').css({position: 'fixed', width: $genislik, height: $yukseklik , left:0, top:0,'z-index':'-1'}).hide())
                //Dondurme fonksiyonunu calistiriyoruz.
                dondur();
                //Tum resimler bittikten fonksiyonu tekrardan yeniden calistiracak olan fonksiyonu yaziyoruz.
                setInterval(function(){dondur()},varsayilan.resimlerArasiGecis*resimler.length)
                //Hooop dondurmeye baÅŸlÄ±yoruz
                function dondur()
                {
                    $('.ab-resim-tasiyici-ab .abResimler-ab').each(function(i) 
                    {
                        var seciliResim = $(this); 
                        setTimeout(function() 
                        {
                            $(seciliResim).prev().fadeOut(varsayilan.resimEfekleri).end().fadeIn(varsayilan.resimEfekleri).end().fadeOut(varsayilan.resimEfekleri);
                        }, varsayilan.resimlerArasiGecis*i); $(seciliResim).fadeOut()
                    });
                }
                // Resimler donerken kullanici tarayici boyutu ile oynarsa resimleri tekrar boyurlandiriyoruz.
                 $(window).bind('resize',function(){
                    $('.abResimler-ab').css({
                        'width' : $(document).width(),
                        'height' : $(window).height()
                    });
                });
            }
        );
    };
})(jQuery);
